const [...inputs] = document.querySelectorAll('input')
inputs.forEach(function (elem) {
    elem.addEventListener('change', function (e) {
        validate(e.target);
    })
})

let renge = document.querySelector('#rangeDate');
let pain = document.querySelector('.pain');
pain.addEventListener('input', (e) => {
    renge.innerHTML = pain.value
})

function validate(elem) {
    let flag = false;
    if (elem.type === 'email') {
        if (!/^[A-z1-9._]+@[a-z1-9._]+.[a-z]{1,4}$/.test(elem.value)) {
            flag = true;
            
        }
    } else if (elem.type === 'tel') {
        if (!/^\+38\([0-9]{3}\)[0-9]{3}-[0-9]{2}-[0-9]{2}$/.test(elem.value)) {
            flag = true;
        }
    } else if (elem.dataset.userName) {
        if (!/^[А-я]+$/.test(elem.value)) {
            flag = true;
        }
    } else {
        if (elem.value === "") {
            flag = true;
        }
    }
    if (flag) {
        elem.style.border = '1px solid red';
    } else {
        elem.style.border = '1px solid green'
    }
};